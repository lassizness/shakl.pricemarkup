<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\EventManager;
use Bitrix\Main\Web\HttpClient;
use Bitrix\Main\DB\Connection;

class CShaklPriceMarkupHandlers
{
    function OnBuildGlobalMenu(&$aGlobalMenu, &$aModuleMenu)
    {
        $MODULE_ID = 'shakl.pricemarkup';

        // Если раздел не существует, добавляем его
        if (!isset($aGlobalMenu['global_menu_shakl'])) {
            $aGlobalMenu['global_menu_shakl'] = array(
                'menu_id' => 'shakl',
                'text' => 'ШАКЛ',
                'title' => 'Меню модулей ШАКЛ',
                'sort' => 1000,
                'items_id' => 'global_menu_shakl',
                'items' => array()
            );
        }

        // добавляем ссылку на страницу настроек модуля в созданный раздел
        $aModuleMenu[] = array(
            'parent_menu' => 'global_menu_shakl',
            'icon' => '',
            'page_icon' => '',
            'sort' => '200',
            'text' => 'Модуль PriceMarkup',
            'title' => 'Модуль PriceMarkup',
            'url' => '/bitrix/admin/shakl_pricemarkup_settings.php',
            'items_id' => 'menu_shakl_pricemarkup',
            'items' => array(
                array(
                    'text' => 'Настройки',
                    'url' => '/bitrix/admin/shakl_pricemarkup_settings.php',
                    'title' => 'Настройки модуля PriceMarkup',
                    'more_url' => array(),
                    'items_id' => 'menu_shakl_pricemarkup_settings',
                ),
            )
        );
    }
}