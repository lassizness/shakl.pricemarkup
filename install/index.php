<?php

use Bitrix\Main\ModuleManager;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Application;
use Bitrix\Main\EventManager;

class shakl_pricemarkup extends CModule
{
    public function __construct()
    {
        $arModuleVersion = [];
        include(dirname(__FILE__) . "/version.php");
        $this->MODULE_ID = 'shakl.pricemarkup';
        $this->MODULE_VERSION = $arModuleVersion["VERSION"];
        $this->MODULE_VERSION_DATE = $arModuleVersion["VERSION_DATE"];
        $this->MODULE_NAME = "ШАКЛ - Наценка на цены";
        $this->MODULE_DESCRIPTION = "Наценка на цены и представление базовых цен как скидок.";
        $this->PARTNER_NAME = "SHAKL tech";
        $this->PARTNER_URI = "https://shakl.tech";
    }

    public function DoInstall()
    {
        global $APPLICATION;

        if (CheckVersion(ModuleManager::getVersion("main"), "14.00.00")) {
            $this->InstallDB();
            $this->InstallFiles();
            $this->InstallEvents();
            ModuleManager::registerModule($this->MODULE_ID);
        } else {
            $APPLICATION->ThrowException("Ваша система не поддерживает D7");
        }
    }

    public function DoUninstall()
    {
        $this->UnInstallDB();
        $this->UnInstallFiles();
        $this->UnInstallEvents();
        ModuleManager::unRegisterModule($this->MODULE_ID);
    }

    public function InstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        $connection = Application::getConnection();
        // Проверяем, существует ли таблица
        $tableExists = $connection->queryScalar(
            "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '" . $connection->getDatabase() . "' AND table_name = 'shakl_pricemarkup'"
        );

        // Если таблица не существует, создаем ее
        if (!$tableExists) {
            $connection->queryExecute("CREATE TABLE IF NOT EXISTS shakl_pricemarkup (
            ID int(11) NOT NULL AUTO_INCREMENT,
            PRICE_TYPE_ID int(11) NOT NULL,
            MARKUP_PERCENTAGE decimal(10, 2) NOT NULL,
            PRIMARY KEY (ID)
        ) ENGINE=InnoDB DEFAULT CHARSET=utf8;");
        }
    }

    public function UnInstallDB()
    {
        Loader::includeModule($this->MODULE_ID);
        $connection = Application::getConnection();
        // Проверяем, существует ли таблица
        $tableExists = $connection->queryScalar(
            "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '" . $connection->getDatabase() . "' AND table_name = 'shakl_pricemarkup'"
        );

        // Если таблица существует, удаляем ее
        if ($tableExists) {
            $connection->queryExecute("DROP TABLE IF EXISTS shakl_pricemarkup;");
        }
    }

    public function InstallFiles()
    {
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/shakl.pricemarkup/install/components", $_SERVER["DOCUMENT_ROOT"] . "/local/components", true, true);
        CopyDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/shakl.pricemarkup/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin", true, true);
    }

    function InstallEvents()
    {
        Loader::includeModule($this->MODULE_ID);
        EventManager::getInstance()->registerEventHandler("main", "OnBuildGlobalMenu", $this->MODULE_ID,'CShaklPriceMarkupHandlers', 'OnBuildGlobalMenu');
    }

    function UnInstallEvents()
    {
        Loader::includeModule($this->MODULE_ID);
        EventManager::getInstance()->unRegisterEventHandler("main", "OnBuildGlobalMenu", $this->MODULE_ID,'CShaklPriceMarkupHandlers', 'OnBuildGlobalMenu');
    }

    public function UnInstallFiles()
    {
        DeleteDirFilesEx("/local/components/shakl/pricemarkup");
        DeleteDirFiles($_SERVER["DOCUMENT_ROOT"] . "/local/modules/shakl.pricemarkup/install/admin", $_SERVER["DOCUMENT_ROOT"] . "/bitrix/admin");
    }
}