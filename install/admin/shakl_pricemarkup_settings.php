<?php
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_before.php");
require_once($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_admin_after.php");

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Catalog\GroupTable;
use Bitrix\Sale\Location\LocationTable;
use Bitrix\Main\UI\Extension;

Extension::load("ui.buttons");
Extension::load("ui.forms");

if (!$USER->IsAdmin()) {
    $APPLICATION->AuthForm("Доступ запрещен");
}

if (!Loader::includeModule('catalog') || !Loader::includeModule('shakl.pricemarkup') || !Loader::includeModule('sale')) {
    ShowError("Не удалось подключить модули.");
    return;
}

$savedMarkupSettings = json_decode(Option::get('shakl.pricemarkup', 'markupSettings', ''), true) ?: [];

if ($_SERVER["REQUEST_METHOD"] == "POST" && check_bitrix_sessid()) {
    if (isset($_POST["save"])) {
        // Собираем введенные настройки из формы
        $inputSettings = $_POST['markupSettings'] ?? [];
        $uniqueSettings = [];

        // Проверяем уникальность ассоциаций
        foreach ($inputSettings as $setting) {
            $key = $setting['locationCode'] . ':' . $setting['priceTypeName']; // Уникальный ключ для проверки
            if (!empty($setting['locationCode']) && !empty($setting['priceTypeName']) && isset($setting['markupPercentage']) && !isset($uniqueSettings[$key])) {
                // Если такой ассоциации еще нет, сохраняем в массив уникальных настроек
                $uniqueSettings[$key] = $setting;
            }
        }
        // Сохраняем только уникальные настройки
        Option::set('shakl.pricemarkup', 'markupSettings', json_encode(array_values($uniqueSettings)));
    } elseif (isset($_POST["delete"])) {
        // Удаление ассоциации
        unset($savedMarkupSettings[intval($_POST["deleteIndex"])]);
        Option::set('shakl.pricemarkup', 'markupSettings', json_encode(array_values($savedMarkupSettings)));
    } elseif (isset($_POST["add"])) {
        // Добавление пустой ассоциации и сохранение изменений
        $savedMarkupSettings[] = ['locationCode' => '', 'priceTypeName' => '', 'markupPercentage' => ''];
        Option::set('shakl.pricemarkup', 'markupSettings', json_encode($savedMarkupSettings));
    }

    // Перезагружаем настройки после изменений
    $savedMarkupSettings = json_decode(Option::get('shakl.pricemarkup', 'markupSettings', ''), true) ?: [];
}


$priceTypes = GroupTable::getList(['select' => ['ID', 'NAME'], 'order' => ['NAME' => 'ASC']])->fetchAll();
$locations = LocationTable::getList([
    'filter' => ['=NAME.LANGUAGE_ID' => LANGUAGE_ID, '=TYPE.CODE' => 'CITY'],
    'select' => ['CODE', 'CITY_NAME' => 'NAME.NAME'],
    'order' => ['NAME.NAME' => 'ASC']
])->fetchAll();

$aTabs = [['DIV' => 'edit1', 'TAB' => 'Настройки', 'TITLE' => 'Настройки модуля наценки цен']];
$tabControl = new CAdminTabControl("tabControl", $aTabs);
?>

    <form method="POST" action="<?= $APPLICATION->GetCurPage() ?>?lang=<?= LANG ?>">
        <?= bitrix_sessid_post() ?>
        <?php $tabControl->Begin(); $tabControl->BeginNextTab(); ?>

        <?php foreach ($savedMarkupSettings as $index => $setting): ?>
            <div style="margin-bottom: 10px;">
                <select name="markupSettings[<?= $index ?>][locationCode]" style="margin-right: 5px;">
                    <option value="">Выберите город</option>
                    <?php foreach ($locations as $location): ?>
                        <option value="<?= htmlspecialcharsbx($location['CODE']) ?>" <?= ($setting['locationCode'] == $location['CODE'] ? 'selected' : '') ?>>
                            <?= htmlspecialcharsbx($location['CITY_NAME']) ?>
                        </option>
                    <?php endforeach; ?>
                </select>

                <select name="markupSettings[<?= $index ?>][priceTypeName]" style="margin-right: 5px;">
                    <option value="">Выберите тип цены</option>
                    <?php foreach ($priceTypes as $priceType): ?>
                        <option value="<?= htmlspecialcharsbx($priceType['NAME']) ?>" <?= ($setting['priceTypeName'] == $priceType['NAME'] ? 'selected' : '') ?>>
                            <?= htmlspecialcharsbx($priceType['NAME']) ?>
                        </option>
                    <?php endforeach; ?>
                </select>

                <input type="text" name="markupSettings[<?= $index ?>][markupPercentage]" value="<?= htmlspecialcharsbx($setting['markupPercentage']) ?>" placeholder="Наценка %" style="margin-right: 5px;">

                <button type="submit" name="delete" value="delete" onclick="return confirm('Удалить ассоциацию?');" style="background-color: red; color: white;">Удалить</button>
                <input type="hidden" name="deleteIndex" value="<?= $index ?>">
            </div>
        <?php endforeach; ?>

        <div>
            <input type="submit" name="add" value="Добавить ассоциацию" class="ui-btn ui-btn-primary" style="margin-right: 5px;">
            <input type="submit" name="save" value="Сохранить изменения" class="ui-btn ui-btn-success">
        </div>

        <?php $tabControl->End(); ?>
    </form>

<?php
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/epilog_admin.php");
