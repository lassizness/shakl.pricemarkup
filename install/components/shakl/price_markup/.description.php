<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentDescription = array(
    "NAME" => "Цена с наценкой",
    "DESCRIPTION" => "Отображает цену без наценки и с наценкой",
    "PATH" => array(
        "ID" => "shakl",
        "NAME" => "SHAKL"
    ),
);
