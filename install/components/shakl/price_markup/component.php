<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

use Bitrix\Main\Loader;
use Bitrix\Main\Config\Option;
use Bitrix\Catalog\PriceTable;
use Bitrix\Sale\Location\LocationTable;

if (!Loader::includeModule('catalog') || !Loader::includeModule('shakl.pricemarkup') || !Loader::includeModule('sale')) {
    ShowError("Модули не найдены.");
    return;
}

$productId = isset($arParams['PRODUCT_ID_VAR']) ? (int)$arParams['PRODUCT_ID_VAR'] : 0;
$cityName = trim($arParams['CITY_VAR']); // Получаем название города из параметров
$roundPrices = $arParams['ROUND_PRICES'] == "Y";
$roundTo = $arParams['ROUND_TO'];

if ($productId <= 0) {
    ShowError("ID товара не указан.");
    return;
}

// Получаем код города по его названию
$cityCode = '';
$cityRes = LocationTable::getList([
    'filter' => ['=NAME.LANGUAGE_ID' => LANGUAGE_ID, '=NAME.NAME' => $cityName, '=TYPE.CODE' => 'CITY'],
    'select' => ['CODE']
]);
if ($cityItem = $cityRes->fetch()) {
    $cityCode = $cityItem['CODE'];
}

if (!$cityCode) {
    ShowError("Код города для " . $cityName . " не найден.");
    return;
}

// Десериализуем настройки из модуля
$markupsSettings = json_decode(Option::get('shakl.pricemarkup', 'markupSettings', ''), true);

// Ищем наценки для текущего города по его коду
$cityMarkups = array_filter($markupsSettings, function($markup) use ($cityCode) {
    return $markup['locationCode'] === $cityCode;
});

if (empty($cityMarkups)) {
    ShowError("Наценки для города " . $cityName . " не найдены.");
    return;
}

$arResult['PRICES'] = [];
$arResult['CITY'] = $cityName; // Передаем название города в результат для доступа в шаблоне

$prices = PriceTable::getList([
    'filter' => ['=PRODUCT_ID' => $productId],
    'select' => ['PRICE', 'CATALOG_GROUP_CODE' => 'CATALOG_GROUP.NAME']
])->fetchAll();

foreach ($prices as $price) {
    foreach ($cityMarkups as $markupSetting) {
        if ($markupSetting['priceTypeName'] === $price['CATALOG_GROUP_CODE']) {
            $markup = (float)$markupSetting['markupPercentage'];
            $priceWithMarkup = $price['PRICE'] * (1 + $markup / 100);

            if ($roundPrices) {
                $priceWithMarkup = $roundTo === "UP" ? ceil($priceWithMarkup / 10) * 10 : floor($priceWithMarkup / 10) * 10;
            }

            $arResult['PRICES'][] = [
                'ORIGINAL_PRICE' => $price['PRICE'],
                'MARKUP' => $markup,
                'FINAL_PRICE' => $priceWithMarkup,
                'PRICE_TYPE_NAME' => $price['CATALOG_GROUP_CODE']
            ];
            break; // Переходим к следующей цене, если наценка найдена
        }
    }
}

$this->IncludeComponentTemplate();