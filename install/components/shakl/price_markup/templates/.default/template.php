<?php if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

foreach ($arResult['PRICES'] as $priceInfo) {
    echo "Тип цены: {$priceInfo['PRICE_TYPE_NAME']}<br>";
    echo "Исходная цена: {$priceInfo['ORIGINAL_PRICE']}<br>";
    echo "Наценка: {$priceInfo['MARKUP']}%<br>";
    echo "Цена с наценкой: {$priceInfo['FINAL_PRICE']}<br><br>";
}
