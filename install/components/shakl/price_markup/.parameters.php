<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arComponentParameters = [
    "GROUPS" => [],
    "PARAMETERS" => [
        "PRODUCT_ID_VAR" => [
            "PARENT" => "BASE",
            "NAME" => "Переменная с ID товара",
            "TYPE" => "STRING",
            "DEFAULT" => '={$_REQUEST["PRODUCT_ID"]}',
        ],
        "CITY_VAR" => [
            "PARENT" => "BASE",
            "NAME" => "Переменная с городом",
            "TYPE" => "STRING",
            "DEFAULT" => '',
        ],
        "ROUND_PRICES" => [
            "PARENT" => "BASE",
            "NAME" => "Включить округление цен",
            "TYPE" => "CHECKBOX",
            "DEFAULT" => "N",
        ],
        "ROUND_TO" => [
            "PARENT" => "BASE",
            "NAME" => "Направление округления",
            "TYPE" => "LIST",
            "VALUES" => ["UP" => "Вверх до ближайшего десятка", "DOWN" => "Вниз до ближайшего десятка"],
            "DEFAULT" => "UP",
            "ADDITIONAL_VALUES" => "N",
        ],
    ],
];

